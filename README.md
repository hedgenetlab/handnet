# handnet
A recurrent neural network that can recognize poker hands

## Description
Handnet is a simple script that trains an LSTM to recognize poker hands. This is
designed to be a self-learning project on ML, pytorch, LSTM and the training
of models on GPU.

## Objective
1. Be able to create a simple model for the hand recognition problem using
pytorch
2. Train the model using the pytorch CUDA API.

## Data
The data used for the project can be found here: https://archive.ics.uci.edu/ml/machine-learning-databases/poker/

## Author
By Jonathan Pelletier (jonathan.pelletier1@gmail.com)
