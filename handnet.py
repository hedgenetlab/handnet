#!/usr/bin/env python
# -*- coding: utf-8 -*-

import torch
import torch.nn as nn

import numpy as np

from torch.autograd import Variable

idx2hand = {
            0:'nothing in hand',
            1:'one pair',
            2:'two pairs',
            3:'three of a kind',
            4:'straight',
            5:'flush',
            6:'full house',
            7:'four of a kind',
            8:'straight flush',
            9:'royal flush'
           }

def training_set(filename):
    """ Loads the training set from a file """

    # training set needs to be a tensor of dimension (timesteps, nbr_batches, 1)
    data = torch.from_numpy(np.loadtxt(filename,delimiter=',',dtype=float).T).unsqueeze(2)
    X = data[:-1,:,:]
    y = data[-1:,:,:]

    return X, y.long().squeeze()

def train_test(X,y,fraction):

    size = X.size(1)
    qty = int(size * fraction)

    if qty == 0:
        qty = 1

    idx = np.random.choice(range(size), qty, replace=False)

    X_test = X[:,idx,:]
    y_test = y[torch.from_numpy(idx)]

    X_train = torch.from_numpy(np.delete(X.numpy(),idx,axis=1))
    y_train = torch.from_numpy(np.delete(y.numpy(),idx,axis=0))

    return X_train, X_test, y_train, y_test

def prepare_data(filename, test_fraction):
    fraction = 0.05
    X, y = training_set(filename)
    return train_test(X,y,fraction=fraction)

def draw_data(X,y,fraction):

    size = X.size(1)
    qty = int(size * fraction)

    if qty == 0:
        qty = 1

    idx = np.random.choice(range(size), qty, replace=False)

    X_draw = X[:,idx,:]
    y_draw = y[torch.from_numpy(idx)]

    return X_draw, y_draw

class Handnet(nn.Module):

    def __init__(self,i,o,h,n):

        super(Handnet, self).__init__()
        # input size
        self.i = i
        # output size (the number of classes)
        self.o = o
        # hidden layer size
        self.h = h
        # number of layers
        self.n = n

        self.lstm = nn.LSTM(self.i, self.h, self.n)
        self.linear = nn.Linear(self.h, self.o)

        return

    def _init_state(self, size):

        hs = Variable( torch.zeros(self.n, size, self.h).double(), requires_grad=False)
        cs = Variable( torch.zeros(self.n, size, self.h).double(), requires_grad=False)

        if torch.cuda.is_available():
            hs = hs.cuda()
            cs = cs.cuda()

        return hs, cs

    def forward(self, x):

        out_lstm, (hs, cs) = self.lstm(x)
        in_linear = out_lstm[-1]

        out_linear = self.linear(in_linear)
        return out_linear

def accuracy(X, y):
    size = X.size(0)
    values, idx = torch.max(X,dim=1)

    acc = torch.sum(idx == y) / size

    return acc

def handnet(
        out_size,
        hidden_size,
        num_layers,
        data_filename,
        test_fraction,
        training_epochs):

    X_train, X_test, y_train, y_test = prepare_data(data_filename, test_fraction)

    X_test = Variable( X_test, requires_grad = False )
    y_test = Variable( y_test, requires_grad = False )

    if torch.cuda.is_available():
        X_test = X_test.cuda()
        y_test = y_test.cuda()

    i = X_train.size(2)
    o = out_size
    h = hidden_size
    n = num_layers

    model = Handnet(i, o, h, n)
    model.double()

    if torch.cuda.is_available():
        model = model.cuda()

    # optimization
    optimizer = torch.optim.RMSprop(model.parameters(), lr=0.08)
    loss_fn = nn.CrossEntropyLoss()

    def closure():
        optimizer.zero_grad()

        X_draw, y_draw = draw_data(X_train, y_train, 0.005)
        X_draw = Variable( X_draw, requires_grad=False)
        y_draw = Variable( y_draw, requires_grad=False)

        if torch.cuda.is_available():
            X_draw = X_draw.cuda()
            y_draw = y_draw.cuda()

        scores = model(X_draw)
        loss = loss_fn(scores, y_draw)
        loss.backward()
        print("loss: {}".format(loss.data[0]))
        return loss

    for i in range(training_epochs):
        print("Training Epoch: {}".format(i))

        optimizer.step(closure)

        # Compute accuracy at the given epoch
        predictions = model(X_test)
        acc = accuracy(predictions.data, y_test.data)
        print('Accuracy on test data:{}'.format(acc))

    return model
